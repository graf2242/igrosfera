from prometheus_client import start_http_server, Summary, Gauge
import random
import psutil
from time import sleep, time
import logging
from logging.handlers import RotatingFileHandler
import argparse

# Create a metric to track time spent and requests made.
PROCESSOR_LOAD = Gauge('load_processor_percent', 'Description of gauge')
PROCESSOR_FREQUENCY = Gauge('load_processor_frequency', 'Description of gauge')
BATTERY_PERCENT = Gauge('load_battery_percent', 'Description of gauge')
MEMORY_USED = Gauge('load_used_memory', 'Description of gauge')
MEMORY_FREE = Gauge('load_free_memory', 'Description of gauge')

FORMAT = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

my_handler = RotatingFileHandler("MetricServer.log", mode='a', maxBytes=5 * 1024 * 1024,
                                 backupCount=50, encoding=None, delay=0)
my_handler.setFormatter(FORMAT)
my_handler.setLevel(logging.INFO)

loggers_names = ["cpu", 'mem', 'battery']
loggers = {}
for logger_name in loggers_names:
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)
    logger.addHandler(my_handler)

    loggers[logger_name] = logger


# Decorate function with metric.
def process_request(t):
    """A dummy function that takes some time."""
    metrics = {
        "proc_percent": psutil.cpu_percent(),
        "frequency": psutil.cpu_freq().current,
        "battery_percent": psutil.sensors_battery().percent if psutil.sensors_battery() else 0,
        "free_mem": psutil.virtual_memory().free,
        "used_mem": psutil.virtual_memory().used,
    }

    PROCESSOR_LOAD.set(metrics["proc_percent"])
    PROCESSOR_FREQUENCY.set(metrics["frequency"])
    BATTERY_PERCENT.set(metrics["battery_percent"])
    MEMORY_FREE.set(metrics["free_mem"])
    MEMORY_USED.set(metrics["used_mem"])

    loggers["cpu"].info("Setting metric {} to {}".format("proc_percent", metrics["proc_percent"]))
    loggers["cpu"].info("Setting metric {} to {}".format("frequency", metrics["frequency"]))
    loggers["battery"].info("Setting metric {} to {}".format("proc_percent", metrics["proc_percent"]))
    loggers["mem"].info("Setting metric {} to {}".format("free_mem", metrics["free_mem"]))
    loggers["mem"].info("Setting metric {} to {}".format("used_mem", metrics["used_mem"]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--timeout', type=float, action='store', dest='timeout', required=False, default=120, help="time to exec")
    options = parser.parse_args()

    start_time = time()
    # Start up the server to expose the metrics.
    start_http_server(9090)
    # Generate some requests.
    while time() - start_time < options.timeout:
        process_request(random.random())
